#!/usr/bin/env python3

import numpy as np
from PIL import Image, ImageTk
import tkinter as tk
import tkinter.filedialog as filedialog

#laplace filter for edge detection (one channel)
def laplace(original):
    channel = (
    -original[0:-2,0:-2] - original[0:-2,1:-1] - original[0:-2,2:]
    -original[1:-1,0:-2] + 9*original[1:-1,1:-1] - original[1:-1,2:]
    -original[2: ,0:-2] - original[2: ,1:-1] - original[2: ,2:]
    )
    return channel

#window class for all operations
class Window:
    #on start show one button for image loading
    def __init__(self, master):
        self.master = master
        master.title("Image editor")
        self.loaded = False
        self.load = tk.Button(master, text = "Load image", command = self.load_image)
        self.load.place(relx = 0.75, rely = 0.1, anchor = "center")

    #show image in the window
    def show_image(self):
        if(self.graycheck == False):
            self.img = Image.fromarray(self.matrix.astype('uint8'), 'RGB')
        else:
            self.img = Image.fromarray(self.matrix.astype('uint8'), 'L')
        thum = self.img.copy()
        #lower preview size
        size = self.master.winfo_width()/2,self.master.winfo_height()/2
        thum.thumbnail(size)
        tkimage = ImageTk.PhotoImage(thum)
        preview = tk.Label(self.master, image=tkimage)
        self.image = tkimage
        preview.place(relx = 0.3, rely = 0.3, anchor = "center")

    #reset image to its original state
    def reset(self):
        self.img = self.oldimg
        self.matrix = np.array(self.img, dtype=np.float)
        self.graycheck = False
        self.show_image()

    #load image and transfrom it into a numpy matrix
    def load_image(self):
        filename = filedialog.askopenfilename(initialdir = "/", title = "Select image",
                                               filetypes = (("All files","*.*"),
                                               ("Common image formats",("*.jpg","*.jpeg","*.png","*.pmp"))))
        self.img = Image.open(filename)
        self.oldimg = self.img.copy()
        self.matrix = np.array(self.img,dtype=np.float)
        self.graycheck = False
        self.show_image()

        #if it is a first image show all other buttons
        if(self.loaded != True):
            self.left = tk.Button(self.master, text = "Rotate left", command = lambda:self.rotate(1))
            self.left.place(relx = 0.2, rely = 0.6, anchor = "center")

            self.right = tk.Button(self.master, text = "Rotate right", command = lambda:self.rotate(3))
            self.right.place(relx = 0.4, rely = 0.6, anchor = "center")

            self.mirrorh = tk.Button(self.master, text = "Vertical mirror", command = lambda:self.mirror(0))
            self.mirrorh.place(relx = 0.3, rely = 0.7, anchor = "center")

            self.mirrorv = tk.Button(self.master, text = "Horizontal mirror", command = lambda:self.mirror(1))
            self.mirrorv.place(relx = 0.3, rely = 0.8, anchor = "center")

            self.reset = tk.Button(self.master, text = "Reset image", command = self.reset)
            self.reset.place(relx = 0.75, rely = 0.2, anchor = "center")

            self.inverse = tk.Button(self.master, text = "Inverse", command = self.inverse)
            self.inverse.place(relx = 0.75, rely = 0.6, anchor = "center")

            self.gray = tk.Button(self.master, text = "Grayscale", command = self.gray)
            self.gray.place(relx = 0.75, rely = 0.9, anchor = "center")

            self.light = tk.Button(self.master, text = "Lighten", command = lambda:self.lighten(1.1,0.7))
            self.light.place(relx = 0.75, rely = 0.7, anchor = "center")

            self.dark = tk.Button(self.master, text = "Darken", command = lambda:self.lighten(0.9,0))
            self.dark.place(relx = 0.75, rely = 0.8, anchor = "center")

            self.edge = tk.Button(self.master, text = "Highlight edges", command = self.edges)
            self.edge.place(relx = 0.3, rely = 0.9, anchor = "center")

            self.save = tk.Button(self.master, text = "Save image", command = self.save)
            self.save.place(relx = 0.75, rely = 0.3, anchor = "center")
        
            self.loaded = True

    #rotate image based on value parameter (clockwise, counterclokwise)
    def rotate(self,value):
        self.matrix = np.rot90(self.matrix,value)
        self.show_image()
   
    #mirror image based on value parameter (horizontally, vertically)
    def mirror(self,value):
        self.matrix = np.flip(self.matrix,value)
        self.show_image()

    #invert image colors
    def inverse(self):
        self.matrix = 255-self.matrix
        self.show_image()

    #convert image to grayscale
    def gray(self):
        if(self.graycheck == False):
            self.matrix = np.round(np.dot(self.matrix[...,:3],[0.229,0.587,0.114]),0)
            self.graycheck = True
            self.show_image()

    #lighten image or darken image (10%, plus is for brightening 0 values)
    def lighten(self,times,plus):
        self.matrix = np.clip(np.round((self.matrix*times)+plus),0,255)
        self.show_image()

    #highlight edges using laplace operator (for grayscale and rgb images)
    def edges(self):
        if(self.graycheck == True):
            newmatrix = np.zeros([self.matrix.shape[0]-2,self.matrix.shape[1]-2])
            newmatrix = laplace(self.matrix)
        else:
            newmatrix = np.zeros([self.matrix.shape[0]-2,self.matrix.shape[1]-2,self.matrix.shape[2]])
            for i in range(3):
                newmatrix[:,:,i] = laplace(self.matrix[:,:,i])
        
        self.matrix = np.clip(newmatrix,0,255)
        self.show_image()

    #save image into a new file
    def save(self):
        if(self.graycheck == False):
            img = Image.fromarray(self.matrix.astype('uint8'), 'RGB')
        else:
            img = Image.fromarray(self.matrix.astype('uint8'), 'L')
        filepath = filedialog.asksaveasfilename(defaultextension = ".jpg")
        img.save(filepath)

#start tkinter
root = tk.Tk()
#set window size
root.geometry("512x512")
#start a new window based on class Window
newwindow = Window(root)
#start tkinter loop
root.mainloop()

