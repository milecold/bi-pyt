# Zde se nachází semestrálka z předmětu BI-PYT #

autor: Oldřich Milec (milecold)

## Zadání ##

 Výstupem semestrální práce bude aplikace napsaná v Python'u, která dokáže načíst vstupní obrázek a na něm provádět vybrané základní grafické operace (následující operace jsou vyžadovány, ale zájemci si mohou seznam libovolně rozšířit):

  *  převrácení obrazu libovolným směrem (o násobky 90 °)
  *  zrcadlení
  *  inverzní obraz;
  *  převod do odstínů šedi
  *  zesvětlení/ztmavení
  *  zvýraznění hran (rozumí se samozřejmě v obraze).

